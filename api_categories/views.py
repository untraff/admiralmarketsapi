from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
import json
from . import solver
from .models import Category


@csrf_exempt
def categories_api_endpoint_view(request):
    if request.method == 'POST':
        s = solver.Solver()
        if s.is_valid_json_data(request.body):
            tree = s.make_tree(s.json_data)
            # [print(row) for row in s.json_tree]
            s.load_json_tree_to_database(tree)
            return HttpResponse('ok')
        else:
            return HttpResponse('json format error!')
    return HttpResponse('categories api')


def categories_api_view(request, category_id):
    try:
        category = Category.objects.get(id=category_id)
    except:
        return HttpResponse('category does not exist!')
    response = {}
    response['id'] = category.id
    response['name'] = category.name
    response['parents'] = []
    response['children'] = []
    response['siblings'] = []

    for sibling in category.get_siblngs():
        response['siblings'].append({'id': sibling.id, 'name': sibling.name})
    for parent in category.get_parents():
        response['parents'].append({'id': parent.id, 'name': parent.name})
    for child in category.get_children():
        response['children'].append({'id': child.id, 'name': child.name})
    return JsonResponse(response, safe=False)
