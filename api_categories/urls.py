from django.urls import path, include
from .views import categories_api_endpoint_view, categories_api_view

urlpatterns = [
    path('', categories_api_endpoint_view, name='categories_api_endpoint_page'),
    path('<int:category_id>/', categories_api_view, name='categories_api_page'),
]
