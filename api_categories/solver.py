import json
import django.db.utils
from .models import Category


class Solver():
    def __init__(self, json_data=None, json_tree=None):
        self.json_data = json_data
        self.json_tree = json_tree

    def is_valid_json_data(self, data):
        """Check data is valid json data and build python_object from it
        """
        try:
            self.json_data = json.loads(data)
            return True
        except json.decoder.JSONDecodeError:
            return False

    def make_tree(self, data, parent=None, result=[]):
        """make tree in python object [parent, child]
        """
        if 'children' in data:
            for child in data['children']:
                tmp = []
                tmp.append(parent)
                tmp.append(data['name'])
                result.append(tmp)
                self.make_tree(child, data['name'], result)
        tmp = []
        tmp.append(parent)
        tmp.append(data['name'])
        result.append(tmp)
        self.json_tree = result
        return result

    def load_json_tree_to_database(self, json_tree):
        for row in json_tree:
            category = Category()
            category.parent_category = row[0]
            category.name = row[1]
            try:
                category.save()
            except django.db.utils.IntegrityError:
                pass
