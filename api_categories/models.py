from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True, blank=False)
    parent_category = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_siblngs(self):
        query = """
            select * from api_categories_category where parent_category = (
                SELECT parent_category from api_categories_category where id = {}
            ) AND id <> {}
            """.format(self.id, self.id)
        return Category.objects.raw(query)

    def get_parents(self):
        query = """
            WITH RECURSIVE r AS (
            SELECT id, parent_category, name, 0 as level
            FROM api_categories_category
            WHERE id = {}

            UNION

            SELECT api_categories_category.id, api_categories_category.parent_category, api_categories_category.name, level+1
            FROM api_categories_category
                JOIN r
                    ON api_categories_category.name = r.parent_category 
            )

            SELECT * FROM r
            where level > 0;
            """.format(self.id)
        return Category.objects.raw(query)

    def get_children(self):
        query = """
            WITH RECURSIVE r AS (
                SELECT id, parent_category, name, 0 as level
                FROM api_categories_category
                WHERE id = {}

                UNION

                SELECT api_categories_category.id, api_categories_category.parent_category, api_categories_category.name, level+1
                FROM api_categories_category
                    JOIN r
                        ON api_categories_category.parent_category = r.name
            )

            SELECT * FROM r
            where level = 1;
            """.format(self.id)
        return Category.objects.raw(query)
