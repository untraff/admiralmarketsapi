# Generated by Django 2.2.1 on 2019-05-26 22:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api_categories', '0002_auto_20190527_0147'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='parent_categry',
            new_name='parent_category',
        ),
    ]
