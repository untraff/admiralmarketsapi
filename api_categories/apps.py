from django.apps import AppConfig


class ApiCategoriesConfig(AppConfig):
    name = 'api_categories'
